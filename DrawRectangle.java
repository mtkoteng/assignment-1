import java.util.Scanner;

//Small class for validating the inputs
class Validator{
    //Check if the scanner recives a number, return true if it does.
    public boolean validateInput(Scanner sc){
        if (sc.hasNextInt()){
            return true;
        } else {
            return false;
        }
    }
    //Check if the number is positive, return true if it is.
    public boolean checkValidNumber(int number){
        if (number > 0){
            return true;
        } else {
            return false;
        }
    }
}
/**Class drawing rectangle based on user input.
    Draws rectangles of #, if the rectangle is has both sides larger than 6, then it will be
    drawned a smaller rectangle inside. */
public class DrawRectangle{
    public static void main(String[] args){
        Validator validator = new Validator();
        Scanner inputHeight = new Scanner(System.in);
        Scanner inputWidth = new Scanner(System.in);

        System.out.print("Welcome to this rectangle drawing app! \nTo exit the app, type in any letters or non positive numbers.\nTo be able to get a smaller rectangle inside, both the sides of the main rectangle has to be\nlonger than 6. Feel free to test as many times as you want!\n\n");
        //The loop runs as long is getting valid input, and displays the rectangles
        while (inputHeight != null && inputWidth != null){
            System.out.println("Write height of the rectangle. Type in a letter to exit the app.");
            int height = 0;
            //Checking valid input for height, ends the program for invalid input, such as negative numbers and non integers.
            if(validator.validateInput(inputHeight)){
                height = inputHeight.nextInt();
                if (!(validator.checkValidNumber(height))){
                    System.out.println("You printed a negative number. App stopped!");
                    break;
                }
            } else {
                System.out.println("You did not write a number! App stopped!");
                break;
            }

            System.out.println("Write width of the rectangle");
            int width = 0;
            //Checking valid input for width, ends the program for invalid input, such as negative numbers and non integers.
            if(validator.validateInput(inputWidth)){
                width = inputWidth.nextInt();
                if (!(validator.checkValidNumber(width))){
                    System.out.println("You printed a negative number. App stopped!");
                    break;
                }
            } else {
                System.out.println("You did not write a number! App stopped!");
                break;
            }

            //Printing out rectangles based on the inputs
            String output = "";
            for (int i = 0; i < height; i++){
                output += "\n";
                for (int j = 0; j < width; j++){
                    //If it is the first, or the last line in the rectangle, it print out a full line with "#"
                    if (i == 0 || i == height-1){
                        output += "#";
                    //If it is in the middle, then print blank for small rectangles, prints a 
                    //new smaller rectangle inside the bigger rectangle.
                    } else if (!(j == 0 || j == width - 1)) {
                        //For rectangles with one or more side smaller then 6, then no room for a rectangle inside.
                        if (width <= 6 || height <= 6) {
                            output += " ";
                        }
                        //Printing a smaller rectangle, where the sides are 4 "#" shorter than the original.
                        if (width > 6 && height > 6){
                            //Printing # where the i and the j is two rows away from the original rectangle
                            if (((i == 2 || i == height - 3) && (j != 1 && j != width-2))  || ((j == 2 || j == width - 3) && (i != 1 && i != height-2))){
                                output += "#";
                            //Emtpy spaces also inside the new rectangle
                            } else {
                                output += " ";
                            }
                        }
                    } else {
                        output += "#";
                    }
                }
            }
            System.out.print(output + "\n\n");
        }
    }
}